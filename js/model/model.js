/**
 * Created by Samuel on 5/30/2017.
 */
function Character(status, reputation, interact) {
    this.status = status;
    this.reputation = reputation;
    this.interact = function() {

    };
}
function Location(desc, choiceA, choiceB, choiceC) {
    this.description = desc;
    this.choice1 = choiceA;
    this.choice2 = choiceB;
    this.choice3 = choiceC;
}