/**
 * Created by Samuel on 5/30/2017.
 */
var currentBackPack = ['Sword'];
var backPackButtons = ["bp0", "bp1", "bp2","bp3","bp4", "bp5", "bp6", "bp7", "bp8", "bp9"];
var currentHitPoints = 20;
var currentStatus = 'fine';
var locationArray = [];
var personArray = [];
var actionArray = [];

//unused function for logging contents of arrays
function printArrays() {
    for (var i = 0; i <locationArray.length; i++) {
        console.log(JSON.stringify(locationArray[i]));
    }
    for (var k = 0; k <personArray.length; k++) {
        console.log(JSON.stringify(personArray[k]));
    }
    for (var m = 0; m <actionArray.length; m++) {
        console.log(actionArray[m]);
    }
}

//functions to change and display items, item dialog
function backPack(itemNew) {
    for (var i = 0; i < currentBackPack.length; i++) {
        if (currentBackPack[i] !== itemNew) {
            currentBackPack.push(itemNew);
        }
        document.getElementById(backPackButtons[i]).innerText = currentBackPack[i]; // currentBackPack.join('\r\n');
    }
    cleanBackPack();
}
function cleanBackPack() {
    for (var i = 0; i < backPackButtons.length; i++) {
        var a = document.getElementById(backPackButtons[i]);
        var b = jQuery(a).attr("id");
        var c = document.getElementById(b).innerText;

        if (c === null || c === "") {
            a.className = "hidebtn";
        }

        else if (c !== null) {
            a.className = "btn";
        }
    }
}
function openDialog(itemName) {
    switch(itemName) {
        case "Note from BMO":
            document.getElementById('dialog').innerText = "Note from BMO";
            $("#dialog").dialog("open");
            break;
            //return false;
        case "welcome":
            document.getElementById('dialog').innerText = "You have entered the ENCHIRIDION! \n A digital version of Ooo, loosely inspired by the Enchiridion's mystical access to alternate realities.";
            $("#dialog").dialog("open");
            break;
    }
}

//creates objects in model.js constructor and sends them to gameLoop
function addPerson(person) {
    personArray.push(person);
}
function addLocation(location) {
 locationArray.push(location);
}
function addAction(action) {
 actionArray.push(action);
}

//advances game or logs chosen action to the screen
function setLocation(location) {
    for (var i = 0; i < locationArray.length; i++) {
        if (location === locationArray[i]) {
            gameLoop(location);
        }
        for (var k = 0; k < actionArray.length; k++) {
            if (location === actionArray[k]) {
             actionLog(location);
            }
        }
    }
}
function actionLog(location) {
    switch (location) {
        case "Intense training (-5 hp)" :
            countHitPoints(-5);
            document.getElementById('actionlog').innerText = "You trained your bod as hard as you could. (-5 hp)";
            break;
        case "Take a nap (restores HP)" :
            countHitPoints("full");
            break;
    }
}

//these functions calculate and display HP/cond data as the game requires
function countHitPoints(hp) {
    var newHP = currentHitPoints + hp;
    if (hp === "full") {
        currentHitPoints = 20;
    }
    else if (newHP !== currentHitPoints) {
        currentHitPoints = newHP;
    }
    displayStatus();
}
function setCondition(condition) {
    if (condition !== currentStatus) {
        currentStatus = condition;
    }
    displayStatus();
}
function displayStatus() {
    var displayHP = currentHitPoints.toString();
    document.getElementById("finnstatus").innerText = "Hitpoints: " + displayHP + "\r\nCondition: " + currentStatus;
}

//these functions start the game upon page load
function initObj() {
    var PrincessBubblegum = new Character("healthy", 4);
    addPerson(PrincessBubblegum);
    var treehouseIntro = new Location('Treehouse', "Take a nap (restores HP)", "Intense training (-5 hp)", "Go outside");
    addLocation(treehouseIntro);
    addAction("Intense training (-5 hp)");
    addAction("Take a nap (restores HP)");
}
function newGame() {
    backPack("Note from BMO");
    displayStatus();
    setLocation(locationArray[0]);
}

function gameLoop(location) {
    document.getElementById('flavortext').innerText = location.description;
    document.getElementById('choice1').innerText = location.choice1;
    document.getElementById('choice2').innerText = location.choice2;
    document.getElementById('choice3').innerText = location.choice3;

    jQuery(".btn").click(function() {
        var bpSlot = jQuery(this).attr("id");
        var itemName = document.getElementById(bpSlot).innerText;
        console.log(itemName + " in slot " + bpSlot);
        openDialog(itemName);
    });

    jQuery(".c-btn").click(function() {
        var choice = jQuery(this).attr("id");
        var advance = document.getElementById(choice).innerText;
        console.log("advancing to " + advance);
        setLocation(advance);
    });
}

function main() {
    //defines the dialog text box that gives player item descriptions or note transcripts to read for clues
    $("#dialog").dialog({
        autoOpen : false, modal : true, show : "blind", hide : "blind", draggable : true, dialogClass: 'textbox'
    });
    openDialog("welcome");
    initObj();
    printArrays();
    newGame();
}

$(document).ready(main());